#!/bin/bash

# Set variables
MR_ID=$1
PROJECT_ID=$CI_PROJECT_ID
ACCESS_TOKEN=$SK_CI_JOB_TOKEN
SERVICE_NOW_INSTANCE=$SK_SERVICENOW_INSTANCE_FULL
USERNAME=$SK_SERVICENOW_USERNAME
PASSWORD=$SK_SERVICENOW_PASSWORD
CHANGE_REQUEST_ID=$2

# Check if the job failed
if [ "$CI_JOB_STATUS" == "success" ]; then
  # Add a comment to the merge request
  COMMENT="✅ GitLab CI/CD Pipeline Passed Successfully!"
  curl --request POST --header "PRIVATE-TOKEN: $ACCESS_TOKEN" --data "body=$COMMENT" "https://gitlab.com/api/v4/projects/$PROJECT_ID/merge_requests/$MR_ID/notes"


  # Update comments in the ServiceNow Change 
  COMMENT_MESSAGE="✅  GitLab CI/CD Pipeline Passed Successfully!"
  SERVICE_NOW_API="https://${SERVICE_NOW_INSTANCE}/api/now/table/change_request/$CHANGE_REQUEST_ID"

  curl --request PUT \
    --url "${SERVICE_NOW_API}" \
    --user "${USERNAME}:${PASSWORD}" \
    --header "Accept: application/json" \
    --header "Content-Type: application/json" \
    --data "{
      \"comments\": \"$COMMENT_MESSAGE\"
    }"
fi
