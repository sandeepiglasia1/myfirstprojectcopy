#!/bin/bash

# Usage: ./fetch_from_servicenow.sh CHANGE_REQUEST_ID
# Example: ./fetch_from_servicenow.sh d733525f47da35108437f9cf016d4302

#PipelineID=$1
#SERVICE_NOW_INSTANCE='https://dev183160.service-now.com'
#USERNAME='admin'
#PASSWORD='l9L5f@Mg@cFL'

PipelineID=$1
SERVICE_NOW_INSTANCE=$SK_SERVICENOW_INSTANCE_FULL
USERNAME=$SK_SERVICENOW_USERNAME
PASSWORD=$SK_SERVICENOW_PASSWORD

# Fetch short_description from ServiceNow
response=$(curl -s -u "$USERNAME:$PASSWORD" "https://$SERVICE_NOW_INSTANCE/api/now/table/change_request?sysparm_query=implementation_planLIKE${PipelineID}&sysparm_display_value=true&sysparm_limit=1")

echo "$response"
