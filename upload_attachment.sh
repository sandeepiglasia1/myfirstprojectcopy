#!/bin/bash

# ServiceNow credentials
#SN_INSTANCE='dev183160.service-now.com'
#SN_USERNAME='admin'
#SN_PASSWORD='l9L5f@Mg@cFL'
#CHANGE_REQUEST_SYS_ID='95a101a9478731108437f9cf016d4324'
SN_INSTANCE=$SK_SERVICENOW_INSTANCE_FULL
SN_USERNAME=$SK_SERVICENOW_USERNAME
SN_PASSWORD=$SK_SERVICENOW_PASSWORD

LOG_FILE_PATH=$1
CHANGE_REQUEST_SYS_ID=$2

# Check if the log file path is provided
if [ -z "$LOG_FILE_PATH" ]; then
  echo "Error: Log file path not provided."
  exit 1
fi

LOG_FILE_NAME=$(basename "$LOG_FILE_PATH")

# ServiceNow API URL for attachments
SN_ATTACHMENT_API="https://${SN_INSTANCE}/api/now/attachment/file?table_name=change_request&table_sys_id=${CHANGE_REQUEST_SYS_ID}&file_name=${LOG_FILE_NAME}"

# Upload the attachment using curl
curl -X POST \
  -u "${SN_USERNAME}:${SN_PASSWORD}" \
  -H "Content-Type: text/html" \
  -F "file=@${LOG_FILE_PATH}" \
  "$SN_ATTACHMENT_API"

